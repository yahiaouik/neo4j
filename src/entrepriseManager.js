import { loadDB } from '../db-connexion.js'

class EntrepriseManager {

    // Permet d'ajouter une entreprise dans la base de donnée
    async createEntreprise(nom, secteur, description, taille) {
        await loadDB().run('MERGE (e:ENTREPRISE {nom: $nom, secteur: $secteur,description: $description, taille: $taille }) RETURN e',
            { nom: nom, secteur: secteur, description: description, taille: taille })
            .then(result => {
                if (result.records[0]) {
                    console.log("L'entreprise " + nom + " a été crée ");
                } else {
                    console.log("L'entreprise " + nom + " n'a pas été crée ");
                }
            });
    }

    // Permet de rechercher une entreprise par nom
    async findEntrepriseByName(nom) {
        await loadDB().run('MATCH (e:ENTREPRISE {nom: $nom}) RETURN e;', { nom: nom })
            .then(result => {
                if (result.records[0]) {
                    console.log("Voici les informations de " + nom + " : ");
                    console.log(result.records[0].get(0).properties);
                } else {
                    console.log("L'entreprise n'a pas été trouvé ");
                }
            })
    }

}

export default new EntrepriseManager();