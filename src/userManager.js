import { loadDB } from '../db-connexion.js'

class UserManager {

    // Permet d'ajouter un utilisateur à la base de données
    async createUser(nom, prenom, description, competences) {
        await loadDB().run('MERGE (u:USER {nom: $nom, prenom: $prenom,description: $description, competences: $competences }) RETURN u;',
            { nom: nom, prenom: prenom, description: description, competences: competences })
            .then(result => {
                if (result.records[0]) {
                    console.log("L'utilisateur " + nom + " " + prenom + " a été crée ");
                } else {
                    console.log("L'utilisateur " + nom + " " + prenom + " n'a pas pu etre crée ");
                }
            });
    }

    // Permet de creer un lien entre un utilisateur et une entreprise
    async createEntrepriseLink(nom_user, prenom_user, entreprise, date_debut, date_fin, poste) {
        await loadDB().run('MATCH (u:USER {nom: $nom_user, prenom: $prenom_user}) MATCH (e:ENTREPRISE {nom: $entreprise}) CREATE (u)-[w:WORKS_FOR {date_debut: $date_debut, date_fin : $date_fin, poste :$poste}]->(e) RETURN u,w,e',
            { nom_user: nom_user, prenom_user: prenom_user, entreprise: entreprise, date_debut: date_debut, date_fin: date_fin, poste: poste })
            .then(result => {
                if (result.records[0]) {
                    console.log("Le lien utilisateur-entreprise a été crée "+ nom_user +" "+ prenom_user+ " --> "+entreprise);
                } else {
                    console.log("Le lien utilisateur-entreprise n'a pas été crée ");
                }
            })
    }

    // Permet de creer un lien de "collègue" entre 2 utilisateurs
    async createUserWorkLink(nom_user_1, prenom_user_1, nom_user_2, prenom_user_2) {
        await loadDB().run('MATCH (u1:USER {nom: $nom_user_1, prenom: $prenom_user_1}) MATCH (u2 :USER {nom: $nom_user_2, prenom: $prenom_user_2}) CREATE (u1)-[w:WORKED_WITH]->(u2) RETURN u1,w,u2',
            { nom_user_1: nom_user_1, prenom_user_1: prenom_user_1, nom_user_2: nom_user_2, prenom_user_2: prenom_user_2 })
            .then(result => {
                if (result.records[0]) {
                    console.log("Le lien collègue a été crée "+ nom_user_1 +" "+ prenom_user_1+ " --> "+ nom_user_2 +" "+prenom_user_2);
                } else {
                    console.log("Le lien collègue n'a pas été crée ");
                }
            })

    }

    // Permet de creer un lien de "connaissance" entre 2 utilisateurs
    async createUserLink(nom_user_1, prenom_user_1, nom_user_2, prenom_user_2) {
        await loadDB().run('MATCH (u1:USER {nom: $nom_user_1, prenom: $prenom_user_1}) MATCH (u2 :USER {nom: $nom_user_2, prenom: $prenom_user_2}) CREATE (u1)-[w:KNOW]->(u2) RETURN u1,w,u2',
            { nom_user_1: nom_user_1, prenom_user_1: prenom_user_1, nom_user_2: nom_user_2, prenom_user_2: prenom_user_2 })
            .then(result => {
                if (result.records[0]) {
                    console.log("Le lien connaissance a été crée " + nom_user_1 +" "+ prenom_user_1+ " --> "+ nom_user_2 +" "+prenom_user_2);
                } else {
                    console.log("Le lien connaissance n'a pas été crée ");
                }
            })
    }

    // Permet de rechercher un utilisateur par nom et prenom
    async findUserByName(nom, prenom) {
        await loadDB().run('MATCH (u:USER {nom: $nom, prenom: $prenom}) RETURN u;', { nom: nom, prenom: prenom })
            .then(result => {
                if (result.records[0]) {
                console.log("Voici les informations de "+ nom+" " + prenom + " : ");
                console.log(result.records[0].get(0).properties);
                } else {
                    console.log("L'utilisateur n'a pas été trouvé ");
                }
            })
    }

    // Permet de suggerer, a un utilisateur donné, des utilisateurs ayant travaillé en meme temps dans une entreprise donnée
    async suggestColaborator(nom, prenom, entreprise) {
        await loadDB().run('MATCH (u1:USER {nom: $nom, prenom: $prenom})-[w1:WORKS_FOR]->(e:ENTREPRISE)<-[w2:WORKS_FOR]-(u2:USER) WHERE NOT (date(w1.date_debut) >= date(w2.date_fin) OR date(w2.date_debut) >= date(w1.date_fin)) RETURN u2',
            { nom: nom, prenom: prenom, entreprise: entreprise })
            .then(result => {
                if (result.records[0]) {
                    console.log("Voici la liste des collègues que " + nom + " " + prenom +" pourrait connaitre :");
                    for( let i=0; i<result.records.length; i++){
                        console.log(result.records[i].get(0).properties);
                    }
                } else {
                    console.log("personne n'a travaillé à " + entreprise + " en meme temps que " + nom + " " + prenom);
                }
            })

    }

    // Permet de suggerer, a un utilisateur donné, les connaissances de leurs connaissances ( que l'utilisateur ne connait pas deja!)
    async suggestRelation(nom, prenom) {
        await loadDB().run('MATCH (u1:USER {nom: $nom, prenom: $prenom})-[w1:KNOW]-(u2:USER)-[w2:KNOW]-(u3:USER) WHERE NOT EXISTS ((u1)-[:KNOW]->(u3))RETURN u3', { nom: nom, prenom: prenom })
            .then(result => {
                if (result.records[0]) {
                    console.log("Voici la liste des personnes que " + nom + " " + prenom +" pourrait connaitre :");
                    for( let i=0; i<result.records.length; i++){
                        console.log(result.records[i].get(0).properties);
                    }
                } else {
                    console.log("Il n'y a pas de relation à proposer");
                }
            })
    }

}

export default new UserManager();