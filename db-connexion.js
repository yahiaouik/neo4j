import neo4j from "neo4j-driver" 
let session;
const url = 'neo4j://localhost:7687'

// Creation de la connection à la base de donnée
export function loadDB () {
    if (session) {
        return session;
    }
    try {
        const driver = neo4j.driver(url, neo4j.auth.basic('neo4j', 'password'));
        session = driver.session();
        
    } catch (err) {
        Raven.captureException(err);
    }
    return session;
};

// Supprime le contenu de la db
export async function cleanDB() {
    await loadDB().run('MATCH (a) DETACH DELETE a;')
    .then(result => { 
        console.log("la base de données a été vidée");
    });
}