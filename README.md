## Projet scolaire Linked-in

### Introduction
Ce projet a pour but de prendre en main la technologie Neo4j.
L'objectif est de représenter des entreprises, des utilisateurs et les différentes relations qui les relient.
Ce projet est a été réalisé en JavaScript grâce à un serveur Nodejs.

### Étapes pour utiliser le code

#### prérequis

Avoir Nodejs et Neo4j d'installer

#### Étapes

Etape 1 : Créer une base de donnée locale (login : neo4j , password: password) et la lancer. ( la bdd doit tourner sur le port 7687)
Etape 2 : Récupérer le code et installer les modules nécessaires : npm install
Etape 3 : Pour lancer le code : npm run start

#### Résultats
Les résultats des différentes fonctions sont visibles dans la console.
Pour une représentation graphique, utiliser Neo4j Desktop.
