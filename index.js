import { cleanDB } from './db-connexion.js'
import entrepriseManager from './src/entrepriseManager.js'
import userManager from './src/userManager.js'

// suppression des infos de la bdd
await cleanDB();

// Céation d'une entreprise
await entrepriseManager.createEntreprise('google', 'informatique','Créateurs du moteur de recherche Google.','85050');
// Recherche de l'entreprise par nom
await entrepriseManager.findEntrepriseByName('google');

// Création d'utilisateurs
await userManager.createUser("yahiaoui","kenza", "dev full stack", ["informatique ", "reseau"]);
await userManager.createUser("tefaatau", "given", "manager", ["informatique ", "reseau"]);
await userManager.createUser("nunes", "martin","dev front end", ["informatique ", "image"]);
await userManager.createUser("saunier", "malo","chef de projet", ["informatique ", "image"]);

// Création d'un lien de connaissance entre les utilisateurs
await userManager.createUserLink("yahiaoui","kenza","nunes","martin");
await userManager.createUserLink("nunes","martin","tefaatau","given");
await userManager.createUserLink("nunes","martin","saunier","malo");
await userManager.createUserLink("yahiaoui","kenza","saunier","malo");

// Recherche d'un utilisateur par nom
await userManager.findUserByName("tefaatau","given");

// Affectation d'un utilisateur à une entreprise pour une periode donnée
await userManager.createEntrepriseLink("yahiaoui","kenza","google", "2019-03-15", "2020-03-15", "employé");
await userManager.createEntrepriseLink("tefaatau","given","google", "2018-03-15", "2019-05-12", "employé");

// Creation d'un lien collègue entre les utilisateurs
await userManager.createUserWorkLink("yahiaoui","kenza","tefaatau","given");

// Suggestion de personne qui ont travaillé en meme temps dans une meme entreprise donnée
await userManager.suggestColaborator("yahiaoui","kenza","google");

// Suggestion relation pour un utilisateur donnée
await userManager.suggestRelation("yahiaoui", "kenza");